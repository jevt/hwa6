import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {



   @Test
   public void testOne(){

      GraphTask.Graph g  = graphForOne();
      List<GraphTask.Vertex> res = g.findSafestRoute("v1","v4");
      System.out.println(g);
      System.out.println("safest: " + res.stream().map(Object::toString).collect(Collectors.joining(" -> ")));
      assertEquals("v1,v2,v3,v4", res.stream().map(Object::toString).collect(Collectors.joining(",")));

   }

   @Test
   public void testTwo(){
      GraphTask.Graph g  = graphForTwo();
      List<GraphTask.Vertex> res = g.findSafestRoute("v1","v4");
      System.out.println(g);
      System.out.println("safest: " + res.stream().map(Object::toString).collect(Collectors.joining(" -> ")));

      assertEquals("v1,v7,v8,v4", res.stream().map(Object::toString).collect(Collectors.joining(",")));

   }


   @Test
   public void testThree(){
      GraphTask.Graph g  = graphForThree();
      List<GraphTask.Vertex> res = g.findSafestRoute("v1","v4");
      System.out.println(g);
      System.out.println("safest: " + res.stream().map(Object::toString).collect(Collectors.joining(" -> ")));

      assertEquals("v1,v2,v7,v8,v3,v4", res.stream().map(Object::toString).collect(Collectors.joining(",")));

   }

   @Test
   public void testFour() {
      GraphTask.Graph g = graphForFour();
      System.out.println(g);
      List<GraphTask.Vertex> result = g.findSafestRoute("v2", "v4");
      System.out.println("safest: " + result.stream().map(Object::toString).collect(Collectors.joining(" -> ")));

      assertEquals("v2,v6,v3,v1,v4", result.stream().map(Object::toString).collect(Collectors.joining(",")));
   }

   @Test
   public void testFive(){
      GraphTask.Graph g  = graphForFive();
      List<GraphTask.Vertex> res = g.findSafestRoute("v1","v7");
      System.out.println(g);
      System.out.println("safest: " + res.stream().map(Object::toString).collect(Collectors.joining(" -> ")));

      assertEquals("v1,v2,v4,v6,v7", res.stream().map(Object::toString).collect(Collectors.joining(",")));

   }

   @Test
   public void shouldThrowExceptionIfFromNotFound() {
      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");

      g.createRandomSimpleGraph(5, 7);

      assertThrows(IllegalArgumentException.class, () -> g.findSafestRoute("badId", "v4"));
   }

   @Test
   public void shouldThrowExceptionIfToNotFound() {
      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");

      g.createRandomSimpleGraph(5, 7);

      assertThrows(IllegalArgumentException.class, () -> g.findSafestRoute("v1", "badId"));
   }

   
   private GraphTask.Graph graphForThree(){

      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[8];
      for(int i = 0; i < 8; ++i) {
         vertices[i] = g.createVertex("v" + (i+1));
      }
      g.connectVertices(vertices[0], vertices[1], 1);
      g.connectVertices(vertices[1], vertices[2], 7);
      g.connectVertices(vertices[2], vertices[3], 2);

      g.connectVertices(vertices[0], vertices[4], 5);
      g.connectVertices(vertices[4], vertices[5], 3);
      g.connectVertices(vertices[5], vertices[3], 10);

      g.connectVertices(vertices[4], vertices[6], 4);
      g.connectVertices(vertices[1], vertices[6], 2);
      g.connectVertices(vertices[6], vertices[7], 3);
      g.connectVertices(vertices[7], vertices[2], 4);
      g.connectVertices(vertices[7], vertices[3], 6);

      g.connectVertices(vertices[4], vertices[5], 3);


      return g;
   }

   private GraphTask.Graph graphForTwo(){
      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[8];
      for(int i = 0; i < 8; ++i) {
         vertices[i] = g.createVertex("v" + (i+1));
      }
      g.connectVertices(vertices[0], vertices[1], 1);
      g.connectVertices(vertices[1], vertices[2], 1);
      g.connectVertices(vertices[2], vertices[3], 5);
      g.connectVertices(vertices[0], vertices[4], 1);
      g.connectVertices(vertices[4], vertices[5], 1);
      g.connectVertices(vertices[5], vertices[3], 10);
      g.connectVertices(vertices[0], vertices[6], 3);
      g.connectVertices(vertices[6], vertices[7], 1);
      g.connectVertices(vertices[7], vertices[3], 4);


      return g;
   }

   private GraphTask.Graph graphForOne(){
      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[6];
      for(int i = 0; i < 6; ++i) {
         vertices[i] = g.createVertex("v" + (i+1));
      }
      g.connectVertices(vertices[0], vertices[1], 2);
      g.connectVertices(vertices[1], vertices[2], 2);
      g.connectVertices(vertices[2], vertices[3], 5);
      g.connectVertices(vertices[0], vertices[4], 1);
      g.connectVertices(vertices[4], vertices[5], 1);
      g.connectVertices(vertices[5], vertices[3], 10);
      return g;
   }



   private GraphTask.Graph graphForFour() {
      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[6];
      for(int i = 0; i < 6; ++i) {
         vertices[i] = g.createVertex("v" + (i+1));
      }
      g.connectVertices(vertices[0], vertices[2], 6);
      g.connectVertices(vertices[0], vertices[3], -10);
      g.connectVertices(vertices[0], vertices[4], -6);
      g.connectVertices(vertices[1], vertices[3], -3);
      g.connectVertices(vertices[1], vertices[5], -10);
      g.connectVertices(vertices[2], vertices[4], -2);
      g.connectVertices(vertices[2], vertices[5], 4);
      g.connectVertices(vertices[3], vertices[5], -3);
      g.connectVertices(vertices[4], vertices[5], -6);
      return g;
   }


   private GraphTask.Graph graphForFive(){
      GraphTask.Graph g = new GraphTask().createGraph("Test Graph");
      GraphTask.Vertex[] vertices = new GraphTask.Vertex[8];
      for(int i = 0; i < 7; ++i) {
         vertices[i] = g.createVertex("v" + (i+1));
      }
      g.connectVertices(vertices[0], vertices[1], 4);
      g.connectVertices(vertices[0], vertices[2], 8);

      g.connectVertices(vertices[1], vertices[3], 3);
      g.connectVertices(vertices[1], vertices[4], 3);

      g.connectVertices(vertices[3], vertices[2], 7);
      g.connectVertices(vertices[2], vertices[5], 1);

      g.connectVertices(vertices[3], vertices[4], 1);
      g.connectVertices(vertices[3], vertices[5], 6);

      g.connectVertices(vertices[4], vertices[6], 7);
      g.connectVertices(vertices[5], vertices[6], 2);





      return g;
   }

}

