import java.util.*;
import java.util.stream.Collectors;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Kirjutada graafi laiuti läbimisel põhinev algoritm, mille käigus leitakse ohutuim tee antud punktist a antud punkti b.
    * Ohutuimaks teeks kahe punkti vahel nimetatakse sellist teed, millel suurim lokaalne langus (punktist järgmisse punkti) on minimaalne
    * */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      if (args.length==0) {
         a.run();
      }else {
         a.runByCmd(args[0],args[1],args[2],args[3]);
      }
   }

   /** Actual main method to run examples and everything. */
   public void run() {

      Graph g = new Graph ("G");

      g.createRandomSimpleGraph (6, 9);

      System.out.println (g);

      List<Vertex> safestRoute = g.findSafestRoute("v1", "v3");

      System.out.println("safest: " + safestRoute.stream().map(Object::toString).collect(Collectors.joining(" -> ")));
   }

   /** Additional method to run program with arguments via command line
    * @param n Number of Vertices
    * @param m Number of Arcs
    * @param to Vertex from
    * @param from Vertex to
    * */
   public void runByCmd(String n, String m, String from, String to) {

      Graph g = new Graph ("G");
      int a = Integer.parseInt(n);
      int b = Integer.parseInt(m);

      g.createRandomSimpleGraph (a, b);

      System.out.println (g);

      List<Vertex> safestRoute = g.findSafestRoute(from, to);

      System.out.println("safest: " + safestRoute.stream().map(Object::toString).collect(Collectors.joining(" -> ")));
   }

   /**
    * For testing, to not make inner classes static
    * */
   public Graph createGraph(String s) {
      return new Graph(s);
   }

   /**
    * Vertex class represents a point on a map
    * */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed
      private int decline;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return String.format("%s[%d]", id, decline);
      }
   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc(String aid, Vertex from, Vertex to, int decline) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         res.decline = decline;
         return res;
      }

      /**
       * Generates decline value that connects to vertices
       * @param v1 Vertex from
       * @param v2 Vertex to
       */
      public void connectVerticesWithRandomDecline(Vertex v1, Vertex v2) {
         connectVertices(v1, v2, new Random().nextInt(10) + 1);
      }


      /**
       * @param v1 Vertex from
       * @param v2 Vertex to
       * @param decline Value of arc
       */
      public void connectVertices(Vertex v1, Vertex v2, int decline) {
         createArc("a" + v1.toString() + "_" + v2.toString(), v1, v2, decline);
         createArc("a" + v2.toString() + "_" + v1.toString(), v2, v1, -decline);
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      public void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               connectVerticesWithRandomDecline(varray[vnr], varray[i]);
            } else {
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            connectVerticesWithRandomDecline(vi, vj);
            connected[i][j] = 1;
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }


      /**
      * Wide-searches graph to find safest route from vFrom to vTo
      * @param vFrom Id of starting Vertex
      * @param vTo Id of end Vertex
      * @return List of safest route
      * @throws IllegalArgumentException if one of vertices not found
      */
      public List<Vertex> findSafestRoute(String vFrom, String vTo) {

         Queue<Route> queue = new LinkedList<>();

         List<Vertex> safestRoute = null;

         int bestDecline = Integer.MAX_VALUE;

         Vertex currentVertex = findVertex(vFrom);

         queue.add(new Route(currentVertex, new LinkedList<>(), Integer.MIN_VALUE));

         Route route;

         List<Vertex> newRoute;

         while (!queue.isEmpty()) {

            route = queue.poll();

            currentVertex = route.nextVertex;

            if (currentVertex.toString().equals(vTo)) {
               route.route.add(currentVertex);

               if (route.maxDecline < bestDecline) {
                  safestRoute = route.route;
                  bestDecline = route.maxDecline;
               }
               continue;
            }

            Arc arc = currentVertex.first;
            while (arc != null) {
               if (!route.isVisited(arc.target)) {
                  newRoute = new ArrayList<>(route.route);
                  newRoute.add(currentVertex);
                  queue.add(new Route(arc.target, newRoute, Math.max(route.maxDecline, arc.decline)));
               }
               arc = arc.next;
            }

         }

         if (safestRoute == null) { throw new IllegalArgumentException("there is no Vertex "+ vTo); }


         return safestRoute;
      }

      /**
       * Finds initial vertex as a starting point
       * @param vId Id of vertex
       * @return Vertex
       */
      private Vertex findVertex(String vId) {
         for (Vertex v = this.first; v != null; v = v.next) {
            if (v.toString().equals(vId)) {
               return v;
            }
         }
         throw new IllegalArgumentException(vId);
      }

      /**
       * Data class to contain information about current route branch
       */
      class Route {
         private Vertex nextVertex;
         private List<Vertex> route;
         private int maxDecline;

         public Route(Vertex nextVertex, List<Vertex> route, int maxDecline) {
            this.nextVertex = nextVertex;
            this.route = route;
            this.maxDecline = maxDecline;
         }

         /**
          * Checks if given route was visited
          * @param v Vertex
          * @return true if list contains Vertex
          */
         boolean isVisited(Vertex v) {
            return route.contains(v);
         }


      }
   }
} 

